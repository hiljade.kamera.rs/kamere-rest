package cmd

import (
	"log"
	"os"

	"gitlab.com/kamere-rest/pkg/rest"

	"github.com/spf13/cobra"
)

var startCmd = &cobra.Command{
	Use:   "start",
	Short: "Spin up REST API server",
	PreRun: func(cmd *cobra.Command, args []string) {
		conf = initConfig(configFile)
	},
	Run: func(cmd *cobra.Command, args []string) {
		// TODO: check error here

		//========================================================================//
		// REST logger
		//========================================================================//
		logFile, err := os.OpenFile(
			"./app.log",
			os.O_CREATE|os.O_APPEND|os.O_WRONLY,
			0600,
		)
		if err != nil {
			log.Fatal(err)
		}

		logger := getLogger("GENERAL", logFile)

		logger.Info().Msg("Starting server")
		db := conf.DBConf.Initialize()
		defer db.Close()

		serverRouter := getLogger("ROUTER", logFile)
		server := rest.Server{
			Conf: &conf.Router,
			Log:  &serverRouter,
			DB:   db,
		}

		log.Fatal(server.Get().ListenAndServe())

		logger.Info().Msg("Stopping server")

	},
}

func init() {}
