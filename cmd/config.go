package cmd

import (
	"log"
	"os"

	"github.com/spf13/viper"
	"gitlab.com/kamere-rest/pkg/db"
	"gitlab.com/kamere-rest/pkg/rest"
)

// Configuration is unmarshalled configuration file
type Configuration struct {
	rest.Router `mapstructure:"router"`
	db.DBConf   `mapstructure:"database"`
}

// initConfig read the configuration file specified by -c
func initConfig(configFile string) (config *Configuration) {
	// open
	confReader, err := os.Open(configFile)
	if err != nil {
		log.Fatalf("error oppening configuration file: %+v", err)
	}
	// read
	if err = viper.ReadConfig(confReader); err != nil {
		if _, ok := err.(viper.ConfigFileNotFoundError); ok {
			log.Fatalf("configuration file not found at %q", configFile)
		} else {
			log.Fatalf("Error reading config file: %+v", err)
		}
	}

	viper.Unmarshal(&config)
	return config
}
