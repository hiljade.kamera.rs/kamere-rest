package cmd

import (
	"fmt"
	"io"
	"os"

	"github.com/rs/zerolog"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

// print usage and exit, use as container for other commands
var rootCmd = cobra.Command{
	Use:   "rest",
	Short: "Kamere REST API server",
	Run: func(cmd *cobra.Command, args []string) {
		cmd.Help()
	},
}

// Persistant flag declarations
var configFile string
var debug bool

var conf *Configuration // placeholder for unmarshalled config

var debugLogger io.Writer // secondary stdin logger

// Execute root of the CMD
func Execute() {
	debugLogger = os.Stdout // add debug writer

	viper.SetConfigType("yaml") // set global viper config type

	rootCmd.AddCommand(startCmd)

	rootCmd.PersistentFlags().StringVarP(
		&configFile,
		"config-file",
		"c",
		"./config.yaml",
		"specify a configuration file to be used",
	)

	rootCmd.PersistentFlags().BoolVarP(
		&debug,
		"debug",
		"d",
		false,
		"run with debug mode",
	)
	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

// getLogger returns zerolog instance w/ debugger if enabled
func getLogger(logType string, writer io.Writer) zerolog.Logger {
	var logOut io.Writer
	if debug {
		logOut = io.MultiWriter(writer, debugLogger)
	} else {
		logOut = writer
	}

	return zerolog.New(logOut).With().Timestamp().Str("Type", logType).Logger()
}
