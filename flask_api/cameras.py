from flask import Blueprint, request
from flask_restful import Resource, Api

from server import db
from models import Camera

camera_blueprint = Blueprint('camera', __name__)
api = Api(camera_blueprint)

@camera_blueprint.route('/v1/cameras', methods=['POST'])
def send_camera():
    data = request.get_json()
    camera = Camera(data.get('lat'), data.get('lon'), data.get('type'))
    db.session.add(camera)
    db.session.commit()
    return {'Status': 'OK'}

@camera_blueprint.route('/v1/cameras', methods=['GET'])
def show_cameras():
    response_object = {
        'status': 'success',
        'data': {
            'camera': [camera.to_json() for camera in Camera.query.all()]
        }
    }
    return response_object, 200

