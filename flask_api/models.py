from server import db


class Camera(db.Model):
    __tablename__ = 'camera'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    latitude = db.Column(db.Float, nullable=False)
    longitude = db.Column(db.Float, nullable=False)
    type = db.Column(db.String(128), nullable=False)

    def __init__(self, latitude, longitude, type):
        self.latitude = latitude
        self.longitude = longitude
        self.type = type

    def to_json(self):
        return {
            'id': self.id,
            'latitude': self.latitude,
            'longitude': self.longitude,
            'type': self.type
        }