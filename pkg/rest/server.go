package rest

import (
	"database/sql"
	"fmt"
	"net/http"
	"time"

	"github.com/rs/zerolog"

	"github.com/gorilla/mux"
)

// Router is used to pass configuration to the rest server handler
type Router struct {
	Hostname     string        `mapstructure:"hostname"`
	Listen       Listen        `mapstructure:"listen"`
	WriteTimeout time.Duration `mapstructure:"write_timeout"`
	ReadTimeout  time.Duration `mapstructure:"read_timeout"`
	API          API           `mapstructure:"api"`
}

// API specific configuration
type API struct {
	Version string `mapstructure:"version"`
}

// Listen defines IP and port for REST server to configed
type Listen struct {
	IP   string `mapstructure:"ip"`
	Port string `mapstructure:"port"`
}

// Server
type Server struct {
	Conf *Router
	Log  *zerolog.Logger
	DB   *sql.DB
}

// Get will take the config passed to it and run the REST server
func (serv *Server) Get() *http.Server {
	//==========================================================================//
	// Server configuration
	//==========================================================================//
	router := mux.NewRouter()
	router.Host(serv.Conf.Hostname)
	apiRouter := router.PathPrefix(serv.Conf.API.Version).Subrouter()
	server := &http.Server{
		Handler:      router,
		Addr:         fmt.Sprintf("%s:%s", serv.Conf.Listen.IP, serv.Conf.Listen.Port),
		WriteTimeout: serv.Conf.WriteTimeout,
		ReadTimeout:  serv.Conf.ReadTimeout,
	}
	//==========================================================================//

	//==========================================================================//
	// Routes
	//==========================================================================//
	apiRouter.HandleFunc("/ping", serv.ping())
	apiRouter.HandleFunc("/cameras", serv.listCameraPositions()).Methods("GET")
	apiRouter.HandleFunc("/cameras", serv.addCamera()).Methods("POST")
	//==========================================================================//

	return server
}
