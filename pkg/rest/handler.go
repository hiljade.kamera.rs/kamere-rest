package rest

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"gitlab.com/kamere-rest/pkg/db"
)

// ping returns pong
func (s *Server) ping() http.HandlerFunc {
	type pingResponse struct {
		Response string `json:"response"`
	}
	return func(w http.ResponseWriter, r *http.Request) {
		json.NewEncoder(w).Encode(pingResponse{Response: "pong"})
	}
}

type camera struct {
	ID        int     `json:"id"`
	Latitude  float64 `json:"lat"`
	Longitude float64 `json:"lng"`
	Type      string  `json:"type"`
}

func (s *Server) listCameraPositions() http.HandlerFunc {
	type listCameraPositionsResponse struct {
		Cameras []camera `json:"cameras"`
	}
	return func(w http.ResponseWriter, r *http.Request) {
		var cameras []camera

		if err := r.ParseForm(); err != nil {
			http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
			return
		}

		rows, err := s.DB.Query(db.ListCameraPositions)
		if err != nil {
			if err == sql.ErrNoRows {
				fmt.Println("Zero rows found")
			} else {
				log.Println(err)
			}
		}
		defer rows.Close()

		for rows.Next() {
			camera := new(camera)

			err := rows.Scan(&camera.ID, &camera.Latitude, &camera.Longitude, &camera.Type)
			if err != nil {
				log.Fatal(err)
			}

			cameras = append(cameras, *camera)
		}

		json.NewEncoder(w).Encode(listCameraPositionsResponse{Cameras: cameras})
	}
}

func (s *Server) addCamera() http.HandlerFunc {
	type addCameraRequest struct {
		Latitude  float64 `json:"lat"`
		Longitude float64 `json:"lng"`
		Type      string  `json:"type"`
	}
	return func(w http.ResponseWriter, r *http.Request) {
		camera := new(addCameraRequest)
		err := json.NewDecoder(r.Body).Decode(&camera)
		if err != nil {
			log.Fatal("Cannot decode request body", err)
		}

		stmt, err := s.DB.Prepare(db.AddCamera)
		if err != nil {
			log.Fatal("Cannot prepare DB statement", err)
		}
		// log.Print(camera)
		res, err := stmt.Exec(camera.Latitude, camera.Longitude, camera.Type)
		if err != nil {
			log.Fatal("Cannot run insert statement", err)
		}

		id, _ := res.LastInsertId()

		log.Printf("Inserted row: %d", id)

		http.Error(w, http.StatusText(http.StatusCreated), http.StatusCreated)
	}
}
