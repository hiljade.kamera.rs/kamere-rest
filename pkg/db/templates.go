package db

var (
	// ListCameraPositions return list of all camera locations
	ListCameraPositions = "SELECT id, latitude, longitude, type FROM camera;"

	AddCamera = "INSERT INTO camera (latitude, longitude, type) values(?, ?, ?);"
)
