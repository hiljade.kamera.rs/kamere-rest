package db

import (
	"database/sql"
	"fmt"
	"log"

	_ "github.com/go-sql-driver/mysql" // importing driver for postgres
)

// DBConf is used to pass configuration to the db handler
type DBConf struct {
	// for unmarshaling configuration file
	IP           string `mapstructure:"ip"`
	Port         string `mapstructure:"port"`
	User         string `mapstructure:"user"`
	Password     string `mapstructure:"password"`
	DatabaseName string `mapstructure:"db_name"`
	// connection itself
	Conn *sql.DB
}

// Initialize the database with given configuration
func (conf *DBConf) Initialize() *sql.DB {
	connectionConfig := fmt.Sprintf(
		"%s:%s@tcp(%s:%s)/%s",
		conf.User,
		conf.Password,
		conf.IP,
		conf.Port,
		conf.DatabaseName,
	)
	// log.Print(connectionConfig)
	log.Printf("%+v\n", conf)
	db, err := sql.Open("mysql", connectionConfig)
	if err != nil {
		log.Fatalf("error while oppening DB: %s", err)
	}

	conf.Conn = db
	return db // so we can defer close on the command
}
